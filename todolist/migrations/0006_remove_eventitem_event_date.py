# Generated by Django 4.1.7 on 2023-05-12 04:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0005_rename_user_id_eventitem_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventitem',
            name='event_date',
        ),
    ]
