from django.contrib import admin

# Register your models here.

from .models import ToDoItem
from .models import EventItem

admin.site.register(ToDoItem)
admin.site.register(EventItem)